﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ_Grocery
{
    public partial class RootForm : Form
    {
        atozDataContext database = new atozDataContext();


        public RootForm()
        {
            InitializeComponent();
        }

        private void RootForm_Load(object sender, EventArgs e)
        {

        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            string username = usernameTextbox.Text;
            string password = passwordTextbox.Text;

            foreach (Admin admin in database.Admins)
            {
                if (username == admin.username && password == admin.password)
                {
                    Dashboard dashboard = new Dashboard();
                    dashboard.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu", "Thông báo");
                }
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void signInButton_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void signInButton_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void signInButton_MouseHover(object sender, EventArgs e)
        {

        }
    }
}
