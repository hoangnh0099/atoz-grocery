﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ_Grocery
{
    public partial class BillForm : Form
    {
        atozDataContext database = new atozDataContext();

        public BillForm()
        {
            InitializeComponent();
        }

        private void BillForm_Load(object sender, EventArgs e)
        {
            renderData();
        }

        private void renderData()
        {
            var query = database.HoaDons.Select(data => new { data.sohd, data.ngaylap, data.tenkh, data.tongthanhtien, data.tensp, data.soluong, data.giasp });
            dataGridView1.DataSource = query;

            var query2 = database.HangHoas.Select(data => new { data.mahh, data.tenhh, data.gia });

            comboBox1.DataSource = query2.ToList();
            comboBox1.ValueMember = "mahh";
            comboBox1.DisplayMember = "tenhh";

            textBox1.Text = "0";
            textBox2.Text = "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int soHoaDon = Convert.ToInt32(sohd.Text);
                DateTime ngayLap = Convert.ToDateTime(dateTimePicker1.Text);
                string donVi = unitInput.Text;
                string tenKhachHang = clientName.Text;
                string tenSanPham = comboBox1.Text;
                int soLuong = Convert.ToInt32(textBox2.Text);
                int giaSanPham = Convert.ToInt32(textBox3.Text);
                int tongTien = Convert.ToInt32(textBox1.Text);

                HoaDon hoaDon = new HoaDon();
                hoaDon.sohd = soHoaDon;
                hoaDon.ngaylap = ngayLap;
                hoaDon.tenkh = tenKhachHang;
                hoaDon.tongthanhtien = tongTien;
                hoaDon.tensp = tenSanPham;
                hoaDon.soluong = soLuong;
                hoaDon.giasp = giaSanPham;

                database.HoaDons.InsertOnSubmit(hoaDon);
                database.SubmitChanges();

                renderData();
            }
            catch (Exception error)
            {
                MessageBox.Show("Thêm mới không thành công, vui lòng kiểm tra lại định dạng nhập");
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.TextLength == 0)
            {
                textBox2.Text = "0";
            }
            else
            {
                int soLuong = Convert.ToInt32(textBox2.Text);
                int giaSanPham = Convert.ToInt32(textBox3.Text);
                int tongTien = soLuong * giaSanPham;

                textBox1.Text = tongTien.ToString();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=DESKTOP-1D0SSQB\SQLEXPRESS;Initial Catalog=QL_hangtaphoa;Integrated Security=True");
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("SELECT * FROM HangHoa WHERE tenhh = '" + comboBox1.Text + "'", sqlConnection);
            command.ExecuteNonQuery();
            SqlDataReader sqlDataReader;
            sqlDataReader = command.ExecuteReader();
            while (sqlDataReader.Read())
            {
                textBox3.Text = sqlDataReader["gia"].ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int selectedData = dataGridView1.CurrentCell.RowIndex;

            HoaDon hoaDon = database.HoaDons.Single(data => data.sohd == Convert.ToInt32(dataGridView1.Rows[selectedData].Cells["sohd"].Value));
            database.HoaDons.DeleteOnSubmit(hoaDon);
            database.SubmitChanges();

            renderData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int soHoaDon = Convert.ToInt32(sohd.Text);
                DateTime ngayLap = Convert.ToDateTime(dateTimePicker1.Text);
                string donVi = unitInput.Text;
                string tenKhachHang = clientName.Text;
                string tenSanPham = comboBox1.Text;
                int soLuong = Convert.ToInt32(textBox2.Text);
                int giaSanPham = Convert.ToInt32(textBox3.Text);
                int tongTien = Convert.ToInt32(textBox1.Text);

                int selectedData = dataGridView1.CurrentCell.RowIndex;

                foreach (HoaDon hoaDon in database.HoaDons.Where(data => data.sohd == Convert.ToInt32(dataGridView1.Rows[selectedData].Cells["sohd"].Value)))
                {
                    hoaDon.ngaylap = ngayLap;
                    hoaDon.tenkh = tenKhachHang;
                    hoaDon.tensp = tenSanPham;
                    hoaDon.soluong = soLuong;
                }
                database.SubmitChanges();

                renderData();
            }
            catch (Exception error)
            {
                MessageBox.Show("Sửa không thành công, vui lòng kiểm tra lại định dạng nhập");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
