﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ_Grocery
{
    public partial class ProductsForm : Form
    {
        atozDataContext database = new atozDataContext();

        public ProductsForm()
        {
            InitializeComponent();
        }

        private void renderData()
        {
            var query = database.HangHoas.Select(data => new { data.mahh, data.tenhh, data.gia, data.donvitinh, data.soluong, data.mancc });

            dataGridView1.DataSource = query;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int productID = Convert.ToInt32(productIDInput.Text);
                string productName = productNameInput.Text;
                int productPrice = Convert.ToInt32(productPriceInput.Text);
                int productQuantity = Convert.ToInt32(productQuantityInput.Text);
                string unit = unitInput.Text;
                string supplier = supplierComboBox.SelectedValue.ToString();

                HangHoa hangHoa = new HangHoa();
                hangHoa.mahh = productID;
                hangHoa.tenhh = productName;
                hangHoa.gia = productPrice;
                hangHoa.soluong = productQuantity;
                hangHoa.donvitinh = unit;
                hangHoa.mancc = supplier;

                database.HangHoas.InsertOnSubmit(hangHoa);
                database.SubmitChanges();

                renderData();
            }
            catch (Exception error)
            {
                MessageBox.Show("Thêm mới không thành công, vui lòng kiểm tra lại định dạng nhập");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string productName = productNameInput.Text;
                int productPrice = Convert.ToInt32(productPriceInput.Text);
                int productQuantity = Convert.ToInt32(productQuantityInput.Text);
                string supplier = supplierComboBox.SelectedValue.ToString();

                int selectedData = dataGridView1.CurrentCell.RowIndex;
                foreach (HangHoa hangHoa in database.HangHoas.Where(data => data.mahh == Convert.ToInt32(dataGridView1.Rows[selectedData].Cells["mahh"].Value)))
                {
                    hangHoa.tenhh = productName;
                    hangHoa.gia = productPrice;
                    hangHoa.soluong = productQuantity;
                    hangHoa.mancc = supplier;
                }
                database.SubmitChanges();

                renderData();
            }
            catch (Exception error)
            {
                MessageBox.Show("Sửa không thành công, vui lòng kiểm tra lại định dạng nhập");
            }
        }

        private void ProductsForm_Load(object sender, EventArgs e)
        {
            var query = database.HangHoas.Select(data => new { data.mahh, data.tenhh, data.gia, data.donvitinh, data.soluong, data.mancc });
            dataGridView1.DataSource = query;

            var query2 = database.NhaCungCaps.Select(data => new { data.mancc, data.tenncc });
            supplierComboBox.DataSource = query2.ToList();
            supplierComboBox.ValueMember = "mancc";
            supplierComboBox.DisplayMember = "tenncc";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int selectedData = dataGridView1.CurrentCell.RowIndex;

            HangHoa hangHoa = database.HangHoas.Single(data => data.mahh == Convert.ToInt32(dataGridView1.Rows[selectedData].Cells["mahh"].Value));
            database.HangHoas.DeleteOnSubmit(hangHoa);
            database.SubmitChanges();

            renderData();
        }
    }
}
